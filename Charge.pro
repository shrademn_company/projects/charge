#-------------------------------------------------
#
# Project created by Shrademn
#
#-------------------------------------------------

QT += core gui widgets
TEMPLATE = app
CONFIG += c++17
DEFINES += QT_IMPLICIT_QFILEINFO_CONSTRUCTION

DESTDIR = $$PWD/build
TARGET = Charge

VERSION = 1.0.0.0
RC_ICONS = $${TARGET}.ico
QMAKE_TARGET_COMPANY = "Shrademn'Company"
QMAKE_TARGET_PRODUCT = "✭"
QMAKE_TARGET_DESCRIPTION = $${TARGET}
QMAKE_TARGET_COPYRIGHT = "☭"

SOURCES += main.cpp \
	MainWindow.cpp

HEADERS += \
	MainWindow.hpp

FORMS += \
    MainWindow.ui

RESOURCES += resources.qrc

DISTFILES +=

include(core/Core.pri)
include(widget/WidgetEnhanced.pri)
include(xlsx/QXlsx.pri)
