//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2020-11-18
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMovie>

#include <Settings.hpp>
#include <TableModel.hpp>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	enum Side
	{
		Aggressor = 0,
		Defender,
		SideCount
	};
	Q_ENUM(Side)
	enum Column
	{
		Name = 0,
		Power,
		Category
	};
	Q_ENUM(Column)
	enum SettingsKey
	{
		AttritionAllowance,
		WinnerAttrition,
		LoserAttrition,
		ExportPath
	};
	Q_ENUM(SettingsKey)

	struct Unit
	{
		Side        side;
		int         index;
		double      power{0};
		QString     type;
		QStringList targets;
	};

	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	void connectActions();
	void connectWidgets();

	void initialize();
	void addUnit(TableModel &model, QString const &name, int power, QString const &category);
	void addSelectedUnits(TableModel &model);

	using Subtargets = QMap<QString, double>;
	using Force = QPair<double, Subtargets>;

	Unit    getUnit(Side side, int index, double power, QString const &category) const;
	Force   total(QList<Unit> const &units);
	QString getContrastCategory(Unit const &unit, Subtargets subtargets);
	double  getContrast(Unit const &unit, QString const &category);
	Force   sideAttack(QList<Unit> const &units, Force target);
	Side    determineWinner(QVector<Force> const &results);
	void    applyAttrition(QList<Unit> units, double power);
	void    autoResolve();

	void load(Side side);
	void save(Side side);

	Settings                  m_settings;
	QMovie                    m_charge{":/charge.gif"};
	TableModel                m_units;
	QVector<TableModel *>     m_sides{new TableModel{this}, new TableModel{this}};
	QMap<QString, Subtargets> m_contrasts;

	static inline QString const s_unit{QStringLiteral("Unit")};
	static inline QString const s_power{QStringLiteral("AI_Combat_Power")};
	static inline QString const s_category{QStringLiteral("CategoryMask")};
	static inline QString const s_filer{QStringLiteral("Charge (*.charge)")};
};

#endif // MAINWINDOW_HPP
