//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2020-11-18
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QFileDialog>
#include <QInputDialog>
#include <QRandomGenerator>

#include <core.tpp>
#include <SApplication.hpp>
#include <xlsxdocument.h>

#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent}
{
	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	m_settings.restoreState(this);
	m_units.addColumns(QList{Name, Power, Category});
	unitTableView->setModel(&m_units);
	m_sides[Aggressor]->addColumns(QList{Name, Power, Category});
	aggressorTableView->setModel(m_sides[Aggressor]);
	m_sides[Defender]->addColumns(QList{Name, Power, Category});
	defenderTableView->setModel(m_sides[Defender]);
	connectActions();
	connectWidgets();
	attritionAllowanceDoubleSpinBox->setValue(m_settings.value(AttritionAllowance, .33).toDouble());
	winnerAttritionDoubleSpinBox->setValue(m_settings.value(WinnerAttrition, .75).toDouble());
	loserAttritionDoubleSpinBox->setValue(m_settings.value(LoserAttrition, .80).toDouble());
	exportPathLineEdit->setText(m_settings.value(ExportPath).toString());
	load(Aggressor);
	load(Defender);
	connect(&m_charge, &QMovie::frameChanged, [this](int frameNumber)
	{
		if (frameNumber == m_charge.frameCount() - 1)
		{
			m_charge.stop();
			label->setMovie({});
		}
	});
	sApp->stopSplashScreen(this);
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
	m_settings.setValue(ExportPath, exportPathLineEdit->text());
	m_settings.setValue(AttritionAllowance, attritionAllowanceDoubleSpinBox->value());
	m_settings.setValue(WinnerAttrition, winnerAttritionDoubleSpinBox->value());
	m_settings.setValue(LoserAttrition, loserAttritionDoubleSpinBox->value());
	save(Aggressor);
	save(Defender);
}

void MainWindow::connectActions()
{
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
	unitTableView->addActions({actionAddAggressorUnits, actionAddDefenderUnits});
	connect(actionAddAggressorUnits, &QAction::triggered, [this]
	{
		addSelectedUnits(*m_sides[Aggressor]);
	});
	connect(actionAddDefenderUnits, &QAction::triggered, [this]
	{
		addSelectedUnits(*m_sides[Defender]);
	});
	aggressorTableView->addActions({actionSave, actionLoad, actionRemove});
	defenderTableView->addActions({actionSave, actionLoad, actionRemove});
	connect(actionLoad, &QAction::triggered, [this]
	{
		auto  *focus{aggressorTableView->hasFocus() ? aggressorTableView : defenderTableView};
		QFile file{QFileDialog::getOpenFileName(this, {}, {}, s_filer)};

		if (file.open(QFile::ReadOnly))
		{
			QDataStream stream{&file};

			*qobject_cast<TableModel *>(focus->proxyModel().sourceModel()) << stream;
		}
	});
	connect(actionSave, &QAction::triggered, [this]
	{
		auto  *focus{aggressorTableView->hasFocus() ? aggressorTableView : defenderTableView};
		QFile file{QFileDialog::getSaveFileName(this, {}, {}, s_filer)};

		if (file.open(QFile::WriteOnly | QFile::Truncate))
		{
			QDataStream stream{&file};

			*qobject_cast<TableModel *>(focus->proxyModel().sourceModel()) >> stream;
		}
	});
	connect(actionRemove, &QAction::triggered, [this]
	{
		auto *focus{aggressorTableView->hasFocus() ? aggressorTableView : defenderTableView};

		qobject_cast<TableModel *>(focus->proxyModel().sourceModel())->removeRows(focus->logicalSelectedRows());
	});
}

void MainWindow::connectWidgets()
{
	connect(exportFilePushButton, &QPushButton::clicked, [this]
	{
		exportPathLineEdit->openBrowser(PathLineEdit::File, "Export File (*xlsx)");
	});
	connect(openFilePushButton, &QPushButton::clicked, [this]
	{
		QDesktopServices::openUrl(QUrl::fromLocalFile(exportPathLineEdit->text()));
	});
	connect(exportPathLineEdit, &QLineEdit::textChanged, this, &MainWindow::initialize);
	connect(autoToolButton, &QToolButton::clicked, this, &MainWindow::autoResolve);
	connect(m_sides[Aggressor], &TableModel::rowsInserted, aggressorTableView, &TableView::resizeColumnsToContents);
	connect(m_sides[Defender], &TableModel::rowsInserted, defenderTableView, &TableView::resizeColumnsToContents);
}

void MainWindow::initialize()
{
	QXlsx::Document    xlsx{exportPathLineEdit->text()};
	int                unitColumn{0};
	int                powerColumn{0};
	int                categoryColumn{0};
	QMap<QString, int> powers;

	m_units.clear();
	for (int column = 1; column <= xlsx.dimension().lastColumn(); ++column)
	{
		if (xlsx.readValue(1, column).toString() == s_unit)
			unitColumn = column;
		else if (xlsx.readValue(1, column).toString() == s_power)
			powerColumn = column;
		else if (xlsx.readValue(1, column).toString() == s_category)
			categoryColumn = column;
	}
	if (unitColumn == 0 || powerColumn == 0)
		return ;
	for (int row = 2; row <= xlsx.dimension().lastRow(); ++row)
	{
		bool ok;
		auto unit{xlsx.readValue(row, unitColumn).toString()};
		int  power{xlsx.readValue(row, powerColumn).toInt(&ok)};

		if (ok)
		{
			addUnit(m_units, xlsx.readValue(row, unitColumn).toString(), xlsx.readValue(row, powerColumn).toDouble(), xlsx.readValue(row, categoryColumn).toString());
			powers[unit] = power;
		}
	}
	for (int side = 0; side < SideCount; ++side)
		for (int row: m_sides[side]->rows())
			m_sides[side]->setData(row, Power, powers.value(m_sides[side]->data(row, Name).toString(), 0));
	unitTableView->resizeColumnsToContents();
	m_contrasts = {
		{"fighter",   {{"fighter", 1   }, {"bomber", 0.25}, {"corvette", 1   }, {"frigate", 1   }, {"capital", 1   }, {"antifighter",  4}}},
		{"bomber",    {{"fighter", 1   }, {"bomber", 1   }, {"corvette", 1   }, {"frigate", 0.25}, {"capital", 0.25}, {"antibomber",   4}}},
		{"corvette",  {{"fighter", 0.25}, {"bomber", 0.25}, {"corvette", 1   }, {"frigate", 1   }, {"capital", 1   }, {"anticorvette", 3}}},
		{"frigate",   {{"fighter", 1   }, {"bomber", 1   }, {"corvette", 0.5 }, {"frigate", 1   }, {"capital", 1   }, {"antifrigate",  3}}},
		{"capital",   {{"fighter", 1   }, {"bomber", 1   }, {"corvette", 0.33}, {"frigate", 0.5 }, {"capital", 1   }, {"anticapital",  2}}},
		{"transport", {{"fighter", 1   }, {"bomber", 1   }, {"corvette", 1   }, {"frigate", 1   }, {"capital", 1   }}}};
}

void MainWindow::addUnit(TableModel &model, QString const &name, int power, QString const &category)
{
	model.insertRow(0);
	model.setData(0, Name, name);
	model.setData(0, Power, power);
	model.setData(0, Category, category);
}

void MainWindow::addSelectedUnits(TableModel &model)
{
	bool ok;
	int  number{QInputDialog::getInt(this, "Add", "Number", 1, 1, 99, 5, &ok)};

	if (ok && unitTableView->selectionModel()->hasSelection())
	{
		QList<TableModel::Row> rows;

		for (int row: unitTableView->logicalSelectedRows())
			rows << m_units.rowData(row);
		while (number-- > 0)
			model.addRows(rows);
	}
}

MainWindow::Unit MainWindow::getUnit(MainWindow::Side side, int index, double power, QString const &category) const
{
	Unit unit{side, index, power, {}, {}};

	for (auto const &type: category.split('|'))
		if (type.contains("anti", Qt::CaseInsensitive))
			unit.targets << type.trimmed().toLower();
		else
			unit.type = type.trimmed().toLower();
	return unit;
}

MainWindow::Force MainWindow::total(QList<Unit> const &units)
{
	Force result;

	for (auto const &unit: units)
	{
		if (!unit.type.isEmpty())
			result.second[unit.type] += unit.power;
		result.first += unit.power;
	}
	return result;
}

QString MainWindow::getContrastCategory(Unit const &unit, Subtargets subtargets)
{
	QString result;
	double  weight{0.};

	for (auto const &category: subtargets.keys())
	{
		double remaining{subtargets[category]};

		if (remaining > 0.)
		{
			double contrastWeight{getContrast(unit, category)};

			if (contrastWeight > 0.)
			{
				double buildWeight{(remaining - unit.power * contrastWeight) / qMax(remaining, unit.power * contrastWeight)};

				buildWeight *= -buildWeight;
				buildWeight += 1.;
				buildWeight = qMax(buildWeight, 0.);
				buildWeight *= contrastWeight;
				if (buildWeight > weight)
				{
					weight = buildWeight;
					result = category;
				}
			}
		}
	}
	return result;
}

double MainWindow::getContrast(Unit const &unit, QString const &category)
{
	if (m_contrasts.contains(category))
	{
		auto const &weights{m_contrasts[category]};
		double     total{0.};
		int        count{0};

		for (auto const &&[type, weight]: keyValues(weights))
		{
			if (unit.targets.contains(type))
				return weight;
			else if (type == unit.type)
				if (weight != 1.)
				{
					total += weight;
					++count;
				}
		}
		if (count)
			return total / count;
	}
	return 1.;
}

MainWindow::Force MainWindow::sideAttack(QList<Unit> const &units, Force target)
{
	for (auto const &unit: units)
	{
		auto category{getContrastCategory(unit, target.second)};
		auto contrast{getContrast(unit, category)};
		auto remainingPower{unit.power * contrast};
		auto modifiedForceApplied{qMin(target.second[category], remainingPower)};

		if (remainingPower)
		{
			remainingPower = (1. - modifiedForceApplied / remainingPower) * unit.power;
			target.second[category] -= modifiedForceApplied;
		}
		target.first -= qMin(target.first, modifiedForceApplied + remainingPower);
	}
	return target;
}

MainWindow::Side MainWindow::determineWinner(QVector<Force> const &results)
{
	auto aggressor{results[Aggressor].first};
	auto defender{results[Defender].first};

	for (auto subresult: results[Aggressor].second)
		aggressor += subresult;
	for (auto subresult: results[Defender].second)
		defender += subresult;
	if (aggressor > defender)
		return Aggressor;
	else if (defender > aggressor)
		return Defender;
	else
		return Aggressor;
}

void MainWindow::applyAttrition(QList<Unit> units, double power)
{
	while (units.size() != 0)
	{
		auto const unit{units.takeAt(QRandomGenerator::global()->generate() % units.count())};

		if (power - unit.power * attritionAllowanceDoubleSpinBox->value() > 0.)
			power = qMax(power - unit.power, 0.);
		else
			m_sides[unit.side]->setData(unit.index, Power, QColor{Qt::red}, Qt::BackgroundRole);
	}
}

void MainWindow::autoResolve()
{
	QVector<QList<Unit>> starting{SideCount};
	QVector<Force>       results{SideCount};
	Side                 winner;

	label->setMovie(&m_charge);
	m_charge.start();
	initialize();
	for (int side = 0; side < SideCount; ++side)
		for (int row = 0; row < m_sides[side]->rowCount(); ++row)
		{
			m_sides[side]->setData(row, Power, {}, Qt::BackgroundRole);
			starting[side] << getUnit(static_cast<Side>(side), row, m_sides[side]->data(row, Power).toDouble(), m_sides[side]->data(row, Category).toString());
		}
	results[Defender] = sideAttack(starting[Aggressor], total(starting[Defender]));
	results[Aggressor] = sideAttack(starting[Defender], total(starting[Aggressor]));
	winner = determineWinner(results);
	winnerLabel->setText(Enum::string(winner) + " wins");
	results[!winner].first += (total(starting[!winner]).first - results[!winner].first) * (1. - loserAttritionDoubleSpinBox->value());
	results[winner].first += (total(starting[winner]).first - results[winner].first) * (1. - winnerAttritionDoubleSpinBox->value());
	for (int index = 0; index < SideCount; ++index)
		applyAttrition(starting[index], results[index].first);
}

void MainWindow::load(MainWindow::Side side)
{
	QDataStream stream{m_settings.value(side).toByteArray()};

	*m_sides[side] << stream;
}

void MainWindow::save(MainWindow::Side side)
{
	QByteArray  array;
	QDataStream stream{&array, QIODevice::WriteOnly};

	*m_sides[side] >> stream;
	m_settings.setValue(side, array);
}
